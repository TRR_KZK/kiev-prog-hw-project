"use strict";
//Task 1
function Human(a, b, c) {
  this.age = a;
  this.firstName = b;
  this.lastName = c;
}

const [human1, human2, human3, human4, human5, human6] = [
  new Human(10),
  new Human(43),
  new Human(76),
  new Human(54),
  new Human(18),
  new Human(26),
];
const testArr = [human1, human2, human3, human4, human5, human6];

testArr.sort(function (a, b) {
  return a.age - b.age;
});
console.log(testArr);


//Task 2
const Human1 = function (birthYear, firstName, lastName) {
  this.birthYear = birthYear;
  this.firstName = firstName;
  this.lastName = lastName;

    if (this.firstName === `John` || this.firstName === `Michael`) this.eyesCol = `green`;
    if (this.lastName === `Smith` || this.lastName === `Sklyar`) this.lovesMusic = true;

    this.calcAge = function () {
    return new Date().getFullYear() - this.birthYear;
}
};

Human1.prototype.job = function () {
   return this.job = `drummer`;
};



const john = new Human1(1988, `John`, `Smith`);
console.log(john.calcAge());
