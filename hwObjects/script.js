"use strict";

const doc = {
    header: undefined, 
    body: undefined, 
    footer: undefined, 
    data: undefined,
    application: {
        header: {},
        body: {},
        footer: {},
        data: {},
    },
    fillingDoc: function (...data) {
        [this.header, this.body, this.footer, this.data] = data;
    },
    fillingApp: function (...data) {
        [this.application.header, this.application.body, this.application.footer, this.application.data ] = data;
        
    },

};

const z = [1, 2, `string`, [5, 6, 7 ,8]];
const x = [1, 2, 3, 4];
const y = [`Space Cowboy`, `Triceratops`, 3, [1, 2, 3, 4, 5]];
doc.fillingDoc(...y);
doc.fillingApp(...z);

console.log(doc);
console.log(doc.application);


