"use strict";
const btn = document.querySelector(`.btn`);

btn.addEventListener(`click`, () => {
  const height = prompt(`Enter circle diameter`, `1 to 20`);
  const width = height;

  if (height > 20) return alert(`Invalid number`);
  for (let i = 1; i <= 100; i++) {
    const circle = document.createElement(`div`);
    document.body.append(circle);
    circle.style.backgroundColor = `#` + Math.trunc(Math.random() * 10000);
    const br = document.createElement(`br`);
    circle.style.height = height + `px`;
    circle.style.width = width + `px`;

    if (
      i === 10 ||
      i === 20 ||
      i === 30 ||
      i === 40 ||
      i === 50 ||
      i === 60 ||
      i === 70 ||
      i === 80 ||
      i === 90
    ) {
      document.body.append(br);
    }
    circle.addEventListener(`click`, () => {
      circle.classList.add(`hidden`);
    });
  }
});
