'use strict';
const simpleArr = [`drums`, `guitars`, `mics`, `saxes`];
let newArray = [];

const funcArr = function (str) {
        return str.split("").reverse().join("");
    
};

const map = function (fn, arr) {
        for (let i = 0; i < arr.length; i++)
                newArray.push(fn(arr[i]));
        return newArray;
         
};

console.log(map(funcArr, simpleArr));

const checkAge = age => age > 18 ? true : confirm(`Родители разрешили?`);
console.log(checkAge(19));
